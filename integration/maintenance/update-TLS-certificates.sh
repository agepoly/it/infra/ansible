#!/bin/sh
# (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
#     Aurélien Martin 2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

BASENAME="$0"

# Error codes
E_USER=1
E_NOCMD=127

die()
{
    retval="$1"; shift
    if [ $# -eq 0 ]; then
        cat <&0 >&2
    else
        printf "%s" "$@" >&2; echo >&2
    fi
    if [ "$retval" = "$E_USER" ]; then
        echo "Run with --help for more information." >&2
    fi
    exit "$retval"
}

usage()
{
  cat <<- EOF
	$BASENAME: Requests new wildcard certificates, update the appropriate
	           TLS secrets and deploy them in a kubernetes cluster.

	Usage:
	  $BASENAME [-h]

	  $BASENAME [--kubeconfig KUBECONFIG_LOCATION] [--tls-secrets-dir TLS_SECRETS_DIR]
	            -d DOMAIN -n K8S_NAMESPACE

	Description:
	  This program uses certbot to request TLS certificates for the specified
	  domain from let's encrypt. These certificates are then moved into the
	  appropriate TLS secrets folder in the ansible repository. The secrets are
	  then applied in the specified kubernetes namespace.


	Options
	  -h, --help
	      Show this help message and exit.
	  -d, --domains
	      Comma separated domains for which to request the wildcard certificates (e.g. ageptest.ch,*.ageptest.ch)
	  -c, --certname
	      Certificate name that will reside on same directory name
	  -e, --email
	      Let's encrypt email address for important account notifications
	  -w, --workdir
	      Let's encrypt path. Default temporary directory
	  -n, --namespace
	      Kubernetes namespace where to apply the secrets.
	  -k, --kubeconfig
	      Use the specified kubectl configuration. If not specified,
	      ~/.kube/config will be used.
	  -s, --tls-secrets-dir
	      Directory where to store the retrieved certificates and key
	  --cert-only
	      Do not install the certificate in kubernetes.
	EOF
}

if [ $# -eq 0 ]; then
    usage;
    exit $E_USER
fi

# Check presence of required binary
for cmd in certbot kubectl; do
  if ! command -v "$cmd" >/dev/null 2>/dev/null; then
    die $E_NOCMD "Could not find command $cmd. Please install it before continuing."
  fi
done

CERT_ONLY=0
K8S_NAMESPACE="default"
KUBECONFIG_LOCATION="$HOME/.kube/config"

while [ $# -gt 0 ]; do
    opt="$1"; shift
    case "$opt" in
        (-h|--help) usage; exit ;;
        (-d|--domains) DOMAINS="$1"; shift ;;
        (-c|--certname) CERT_NAME="$1"; shift ;;
        (-e|--email) LETSENCRYPT_EMAIL="$1"; shift ;;
        (-w|--workdir) WORKDIR=$1; shift ;;
        (-n|--namespace) K8S_NAMESPACE="$1"; shift ;;
        (-k|--kubeconfig) KUBECONFIG_LOCATION="$1"; shift ;;
        (-s|--tls-secrets-dir) TLS_SECRETS_DIR="$1"; shift ;;
        (--cert-only) CERT_ONLY=1 ;;
        (-*) die $E_USER 'Unknown option: %s' "$opt" ;;
        (*) die $E_USER 'Trailing argument: %s' "$opt" ;;
    esac
done

TLS_SECRETS_DIR="${TLS_SECRETS_DIR:-$WORKDIR/tls_secret}"
mkdir -p "$TLS_SECRETS_DIR"

# Functions

request_challenge(){
  echo "Generate certificate $CERT_NAME for $DOMAINS inside $WORKDIR for $LETSENCRYPT_EMAIL"
  certbot -d "$DOMAINS" --manual --preferred-challenges dns certonly \
    --text --agree-tos --email "$LETSENCRYPT_EMAIL" --manual-public-ip-logging-ok --no-eff-email \
    --config-dir "$WORKDIR" --work-dir "$WORKDIR" --logs-dir "$WORKDIR" --cert-name "$CERT_NAME"
}

copy_certificates_to_secrets_dir(){
  cp -Lrv "$WORKDIR/live/$1/" "$TLS_SECRETS_DIR"
}

create_woking_dir(){
  if [ -z "$WORKDIR" ]; then
    TEMPORARY_PATH="$(mktemp -d)"
    WORKDIR=$TEMPORARY_PATH
  fi
  mkdir -p "$WORKDIR"
}

create_k8s_tls_secret(){
  if [ "$CERT_ONLY" -eq "1" ];then
     echo "Kubernetes configuration not applied because --cert-only was specified in the command line."
     return
  fi

  echo "Deleting previously existing secret"
  kubectl delete secret tls "$1" --namespace="$2" || true

  printf "Applying secret: "
  kubectl create secret tls "$1" \
    --kubeconfig="$KUBECONFIG_LOCATION" \
    --namespace="$2" \
    --cert="$3/$1/fullchain.pem" \
    --key="$3/$1/privkey.pem"
}

remove_temp_path(){
  if [ -n "$TEMPORARY_PATH" ]; then
    rm -Rf "$TEMPORARY_PATH"
  fi
}


# Main

echo "Creating working directory."
create_woking_dir "$WORKDIR"
echo "Requesting DNS challenge using Let's encrypt."
request_challenge "$DOMAINS" "$LETSENCRYPT_EMAIL" "$WORKDIR" "$CERT_NAME"
echo "Copying certificate $CERT_NAME from $WORKDIR to secret dir $TLS_SECRETS_DIR."
copy_certificates_to_secrets_dir "$CERT_NAME"
echo "Creating kubernetes TLS secret."
create_k8s_tls_secret "$CERT_NAME" "$K8S_NAMESPACE" "$TLS_SECRETS_DIR"
echo "Deleting temporary working path."
remove_temp_path
echo "Done."
