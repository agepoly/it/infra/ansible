#!/bin/sh
usage()
{
  cat <<- EOF
        $0: Set up vxlan interface integrated to a bridge with unicast lookup table.
        Options
        -h|--help
        -n|--name VXLAN Interface name
        -v|--vni  VNI
        -d|--dev  Physical device for tunnel endpoint communication
        -b|--bridge
        -i|--bridge-ip
        -d|--destination
        --create  Create interface with args
        --delete  Delete interfaces based on args
        --status  Show vxlan and bridge interfaces/address

        Exemple:

        Create VXLAN and bridge
        $0 --create --name vxlan100 --vni 100 --dev wg0 --bridge brvxlan100 --bridge-ip 192.168.200.1/24 --destination "192.168.0.2 192.168.0.3"  

        Delete VXLAN and bridge
        $0 --create --name vxlan100 --vni 100 --dev wg0 --bridge brvxlan100 --bridge-ip 192.168.200.1/24 --destination "192.168.0.2 192.168.0.3"  

        Status of VXLAN and bridges
        $0 --create --name vxlan100 --vni 100 --dev wg0 --bridge brvxlan100 --bridge-ip 192.168.200.1/24 --destination "192.168.0.2 192.168.0.3"  
EOF
}

create_vxlan_bridge()
{
ip link add $VXLAN_NAME type vxlan id $VXLAN_VNI dstport 4789 dev $VXLAN_DEV
ip link add $BRIDGE_NAME type bridge
ip link set $VXLAN_NAME master $BRIDGE_NAME
for i in $BRIDGE_FDB; do
bridge fdb append to 00:00:00:00:00:00 dst $i dev $VXLAN_NAME
done
ip addr add $BRIDGE_IP dev $BRIDGE_NAME
sleep 2
ip link set up dev $VXLAN_DEV
ip link set up dev $BRIDGE_NAME
}

delete_vxlan_bridge()
{
ip link set down dev $BRIDGE_NAME
ip link set down dev $VXLAN_DEV
brctl delif $BRIDGE_NAME $VXLAN_DEV
brctl delbr $BRIDGE_NAME
ip link del $VXLAN_NAME
ip link del $BRIDGE_NAME
}

print_status()
{

echo "[*] Bridge where vxlan interface is connected"
brctl show $BRIDGE_NAME

echo "[*] Lookup table of interface"
bridge fdb show dev $VXLAN_NAME

echo "[*] Link and address"
for i in $VXLAN_NAME $BRIDGE_NAME; do
 ip link show $i
 ip addr show $i
done
}

while [ $# -gt 0 ]; do
    opt="$1"; shift
    case "$opt" in
        (-h|--help) usage; exit ;;
        (-n|--name) VXLAN_NAME="$1"; shift ;;
        (-v|--vni) VXLAN_VNI="$1"; shift ;;
        (-d|--dev) VXLAN_DEV="$1"; shift ;;
        (-b|--bridge) BRIDGE_NAME="$1"; shift ;;
        (-i|--bridge-ip) BRIDGE_IP="$1"; shift ;;
        (-d|--destination) BRIDGE_FDB="$1"; shift ;;
        (--create) CREATE="0" ;;
        (--delete) DELETE="0" ;;
        (--status) STATUS="0" ;;
        (-*) die $E_USER 'Unknown option: %s' "$opt" ;;
        (*) die $E_USER 'Trailing argument: %s' "$opt" ;;
    esac
done

if [ $CREATE ] ; then
create_vxlan_bridge
fi
if [ $DELETE ] ; then
delete_vxlan_bridge
fi
if [ $STATUS ] ; then
print_status
fi

