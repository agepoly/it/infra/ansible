This package contains the certificate authority for AGEPoly-signed certificates.

The certificates here have a default validity of 365 days. See the instructions
stored along the private key to generate a new self-signed certificate. Such
certificate should be moved here and replace any existing certificate in this
directory.

Upon updating the certificate, a new entry in the changelog of the package must
be made:

- Add a new entry to the changelog.
  ```
  debchange -i
  ```

- Release the new version of the package. An editor window will be opened to
  inspect the changelog. You can modify the changelog if needed. Exit the editor
  to continue.
  ```
  debchange -r
  ```

The debian package may be built using the following command:
```
debuild -us -uc
```

The debian package (`agepoly-certificates_X.X.X_all.deb`) will be generated in
the parent directory along with other debian packaging-related files which are
not required and may be deleted.

The debian package should not be commited to this repository.
