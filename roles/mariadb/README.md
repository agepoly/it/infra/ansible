Mariadb role by AGEPoly handle single mariadb instance or master/slave replication DB model for Debian and Ubuntu

The following variables used to tag a node as a master/slave node
It's suited to be used in inventory file, or whereever you want

    mariadb_type : master|slave
    mariadb_replication: bool
    mariadb_db_user_in_file: bool
    mariadb_server_id : int  #It has to be unique per server https://mariadb.com/kb/en/library/replication-and-binary-log-system-variables/#server_id

## General and replication settings

    mariadb_root_password str

    mariadb_replication_user str
    mariadb_replication_pass str
    mariadb_master_ip str

### Backup

Backup are not working for the moment on Ubuntu bionic due to missing mariadb-backup package https://gitlab.com/agepoly/it/infra/ansible/issues/53

Variable used for periodic backup

    mariadb_backup_cron_minutes int
    mariadb_backup_cron_hour int
    mariadb_backup_cron_rsync_minutes int
    mariadb_backup_cron_rsync_hour int
    mariadb_backup yes|no mandatory

    mariadb_server_backup_host str
    mariadb_server_backup_dir str
    mariadb_server_backup_user str

### SSL

Variable used for enable SSL support of mariadb

    mariadb_ssl bool true|false mandatory
    mariadb_ssl_generate_certificate true|false
    mariadb_ssl_force_regenerate bool default false

### Database defininition

mariadb_db_user_in_file: bool

It's possible to define user and database in file to have a unique source of truth.

Just source in your playbook the file with your database and user definition.

    eg:
    vars_file:
      - ../{{ secret_dir }}/mariadb/mariadb_db_user.yml

    mariadb_databases:
      - { name: "db1" , state: "present" }
      - { name: "db2" , state: "present" }
      - { name: "db3" , state: "present" }

    mariadb_users:
        - { name: "user1", password: "xxx", encrypted: "no", priv: "db1.*:ALL", host: "%", state: "present"}
        - { name: "user2", password: "xxx", encrypted: "no", priv: "db2.*:ALL", host: "%", state: "present"}
        - { name: "user3", password: "xxx", encrypted: "no", priv: "db3.*:ALL", host: "%", state: "present"}
