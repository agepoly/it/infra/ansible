The netfilter role install iptables-persistent and deploy iptables rules on target node.

You just need to put rule in file:

../{{ secret_dir }}/netfilter/files/{{ansible_fqdn}}.rules

Replace {{ansible_fqdn}} with the FQDN of the node you want to write the iptable rule

The role will check the ansible_fqdn fact and deploy the proper rule on node
