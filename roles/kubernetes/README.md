# master and worker node kubernetes role

Properties of the role:

* Works with kubernetes >= v1.17.
* Uses kuberouter as CNI.
* Creates production, staging and test k8s namespaces.
* Configures serviceaccount for gitlab integration.
* Works on Debian.

Warning: Cluster (and master node) can be wiped by setting the variable `k8s_reset_cluster`
Warning: Nodes can be wiped by setting the variable `k8s_reset_node`
(False by default).


## Configuration

Master nodes should be part of the `k8s-master` group.
Worker nodes should be part of the `k8s-worker` group.

The following configuration variables MUST be set:

- `kubernetes_config_path`: Path to static cluster configuration.


### Masters-only variables

The following variables are used to bootstrap any master nodes:

- `k8s_api_public_address`: ip/dns where the api can be reached publicly
- `k8s_api_address`: ip/dns where the api can be reached internally (should be a dns for multi-master)
- `k8s_api_port`: ip zone to use for the pod nodes.

- `network_ip`: ip zone to use for the pod nodes.


### Secret variables

The following variables are used to allow the cluster to connect to an external
registry. In particular, they should be set to connect to the gitlab's registry.

- `kubernetes_secret_docker_name`.
- `kubernetes_secret_docker_server`.
- `kubernetes_secret_docker_username`.
- `kubernetes_secret_docker_password`.
- `kubernetes_secret_docker_email`.
