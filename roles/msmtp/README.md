msmtp is an SMTP client that forward email to another mail server that will take care of delivery.
The role install, and deploy configuration. It has only be tested on ubuntu

Defaults variable used by the role can be found under msmtp/defaults/main.yml

This variable is used to specify user(s) that can use the global msmtp configuration to send email

```
msmtp_system_user:
  - 'nagios'
```

For more detail about variable read https://marlam.de/msmtp/msmtp.html#Configuration-files
