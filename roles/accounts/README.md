Ansible role to manage user accounts.

This role installs package `sudo`, creates user accounts and 
configures their respective public keys in their user accounts.

## Create a yaml file with list of users

    users:
      - username: "xxx"
        groups: "sudo"
        shell: "/bin/bash"

## Add in your playbook the path of user file

    vars_files:
      - where_you_store_the_file/users.yml

## Public key 

Set path of public keys with variable "accounts_pubkeys_dir"

Public key name file has to follow this format:

    username.pub 
