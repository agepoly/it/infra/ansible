# postgresql role

Properties of the role:

* Install and configuration of postgresql.
* Add and remove databases and users.
* Periodic database backups.


## Configuration

The following configuration variables MUST be set:

- `pgsql_version_major`: postgresql cluster version.
- `pgsql_version`: postgresql package version.


### Backup configuration

- `pgsql_backup_cron_minutes`: int
- `pgsql_backup_cron_hour`: int
- `pgsql_backup_cron_rsync_minutes`: int
- `pgsql_backup_cron_rsync_hour`: int
- `pgsql_backup`: boolean

- `pgsql_server_backup_dir`.
- `pgsql_server_backup_host`.
- `pgsql_server_backup_user`.

- `pgsql_server_backup_privkey`.
- `pgsql_server_backup_pubkey`.


### Database definitions

The following variables are used for configuring databases:

> ```
> pgsql_databases:
> - { name: "db1", owner: "user1", state: "present" }
> ```

> ```
> pgsql_users:
> - { name: "auremart", db: "db1", password: "xxx", priv: "ALL", state: "present"}
> ```

> ```
> pgsql_pg_hba_custom:
> - { type: "hostssl", database: "all", user: "all", address: "0.0.0.0/0", method: "md5" }
> ```
