# Network interface configuration

Configure network interfaces for AGEPoly physical servers in EPFL MA datacenter.

These machines have a dual-link connection on interfaces `eno1` and `eno2`. This
roles bridges both ports

`external_ifaces_bond_name`: Name of the master interface `eno1` and `eno2` will be slaves of.

`external_ifaces_br_name`: Name of the interface bridged to the bond, which represents a single connection to the outside world.
`external_gw`: IP address of the gateway `eno1` and `eno2` are connected to.
`external_ip`: Public IP assigned to the machine.
`external_ip_mask`: Netmask of the public IP assigned to the machine.
