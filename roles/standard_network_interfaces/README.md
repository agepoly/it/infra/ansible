# Network interface configuration

Configure network interface for AGEPoly vm's after initial install by FAI iso
(Can be use with anything having a single ip on a single iface)

`standard_iface_name`: Name of the inteface to be configured

`temp_ip`: IP of the machine to configure (if not the one set in inventory)
So you can have a FAI giving your machine a bootstrap ip.

`standard_ip_mask`: Netmask of the IP assigned to the machine.
`standard_default_gateway`: default gateway

`dnsserver1`: Primary dns server
`dnsserver2`: Secondary dns server

If used with a new ip, this role need to be used with `gather_facts: no` in playbook (it gather facts after ip correctly set)