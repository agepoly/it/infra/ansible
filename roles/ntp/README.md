NTP role deploy systemd-timesyncd configuration and enable it. 

It remove NTP package that conflict with systemd-timesyncd

## Usefull comand line

Check logs
```
journalctl --since -1h -u systemd-timesyncd
```

Check if clock is synchronize
```
timedatectl status
```

## Ressource:

https://www.freedesktop.org/software/systemd/man/systemd-timesyncd.service.html



