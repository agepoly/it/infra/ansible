# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "generic/debian10"
  [
    ["vgmonitoring", "192.168.40.15"],
    ["vgagepsrv1", "192.168.40.19"],
    ["vgagepsrv2", "192.168.40.20"],
    ["vgagepsrv3", "192.168.40.35"],
    ["vgagepsrv4", "192.168.40.18"],
    ["vgagepbackupnas", "192.168.40.146"],
  ].each do |hostname, ipaddr|
    config.vm.define hostname do |node|
      node.vm.network "private_network", ip: ipaddr
      node.vm.hostname = hostname
      node.vm.provider :libvirt do memory = 1024 end

      # We use hostmanager as a provider to workaround a bug in vagrant-hostmanager:
      # https://github.com/devopsgroup-io/vagrant-hostmanager/issues/277
      node.hostmanager.enabled = false
      node.vm.provision :hostmanager
      node.hostmanager.manage_guest = true
      node.hostmanager.include_offline = true

      node.vm.provision "shell" do |s|
        ssh_pub_key = ""
        if File.file?("#{Dir.home}/.ssh/id_rsa.pub")
          ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
        else
          puts "Failed to locate an SSH key to install in the VM."
        end
        s.inline = <<-SHELL
          mkdir -p /root/.ssh
          chmod 700 /root/.ssh
          chown root:root /root/.ssh
          if grep -sq "#{ssh_pub_key}" /root/.ssh/authorized_keys; then
            echo "SSH keys already provisioned."
          else
            touch /root/.ssh/authorized_keys
            echo #{ssh_pub_key} >> /root/.ssh/authorized_keys
            chmod 644 /root/.ssh/authorized_keys
            echo "SSH key provisionend."
          fi

          # Hostname will be populated by hostmanager
          sed '/127.0.1.1/d' -i /etc/hosts

          # Set root password
          echo root:root | sudo chpasswd
        SHELL
      end
    end
  end
end
