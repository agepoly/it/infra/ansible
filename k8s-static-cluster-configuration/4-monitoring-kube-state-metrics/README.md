## KubeStateMetrics
This folder provide ressources for KubeStateMetrics.
See https://github.com/kubernetes/kube-state-metrics.
It provides metrics on the object inside the cluster,
here is some exemples of recolted metrics : 

namespace : 
kube_namespace_created
kube_namespace_labels
kube_namespace_status_phase
...

pods :
kube_pod_start_time
kube_pod_status_scheduled
...


You can find some useful queries on the github repos /docs section.
