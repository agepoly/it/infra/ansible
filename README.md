# Ansible at AGEPoly

[Ansible](https://www.ansible.com/) as a configuration manager. We use this to
manage and maintain our infrastructure. This repository has the following
objectives:

- Provide a declarative and authoritative representation of the infrastructure
  at AGEPoly.

- Coordinate effors between AGEPoly IT administrators and infra tasks.

- Store and deploy a reproducible configuration.

## Workflow

### Reference workflow for administrators

- Unlock the repository secrets and inventories: `git crypt unlock`. \
  Test ssh access to the hosts: `ansible -i inventories/VPSI-DC-MA-FAC -m ping all`.

- Run playbooks from the `playbooks/` directory.

- Before running a playbook, it's good practice to use `--check` to get an idea
  of the changes to be applied. e.g.:
  ```
  ansible-playbook -i ../inventories/VPSI-DC-MA-FAC srv1.yml --check
  ```

### Contributing

> The vagrant inventory is currently encrypted along the production inventories.
> This is a bug and will be adressed in the future.

- Search in the gitlab [issues](https://gitlab.com/agepoly/it/infra/ansible/issues/)
  for the feature or bug you'd like to work on. If an issue does not exist,
  create a new one; please remember to describe the feature or bug precisely.

- Setup a copy of the AGEPoly infrastructure using [vagrant](#vagrant). See the
  dedicated section in this document for details.

- Develop the desired feature or correct that nasty bug.

- Run the playbook with your changes using the vagrant inventory:
  ```
  ansible-playbook -i ../inventories/Vagrant -u <remote_username> <playbook>.yml
  ```

- Before making a merge request, please destroy and recreate the vagrant domain,
  run your playbook again and verify the changes performed by the playbook are
  as intended.

- Open a merge request against this repository. Follow the instructions in the
  new merge request template.

## Conventions

### Directory organization

```
├── README.md
├── integration                      | Content meant for local use
│   ├── debian-pkgs                  | Debian packages
│   └── fai                          | Debian hosts provisioning automation
├── inventories                      | Ansible inventories
│   ├── xaas-epfl                    | Provided by XaaS EPFL
│   ├── vagrant                      | Provisioned by Vagrant
│   └── VPSI-DC-MA-FAC               | Physical servers
├── k8s-static-cluster-configuration | k8s configuration required to deploy on k8s
├── kryptonite                       | Role secrets (encrypted via git-crypt)
│   ├── accounts                     | AGEPoly IT administrators user accounts
│   ├── netfilter                    | Firewall security policies, per host
│   ├── services                     | AGEPoly k8s applications secrets
│   ├── tls                          | x509 certificates and keys
│   └── ...
├── playbooks
│   ├── ansible.cfg                  | Ansible configuration file (see note bellow).
│   ├── group_vars                   | Group-specific variables
│   ├── host_vars                    | Host-specific variables
│   └── ...
└── roles
    └── ...
```

> We decided to place our playbooks in a directory. This is not common practice.

### Ansible Role Secrets

Secrets related to the deployment are stored in the _kryptonite_ directory.
They are encrypted using [git-crypt](https://github.com/AGWA/git-crypt).

Secret are:
- Security policies
- Software configurations
- Database definitions
- Kubernetes secret
- TLS certificate

We plan to [split configurations and policies](https://gitlab.com/agepoly/it/infra/ansible/issues/45)
one day.

# Vagrant

[Vagrant](https://www.vagrantup.com/) is an open-source software for building
and maintaining portable environments. We provide a [Vagrantfile](https://www.vagrantup.com/docs/vagrantfile/)
that may be used to provide an inventory replicating all of AGEPoly servers
using local virtual machines.

Our vagrant configuration has the following requisites:

- GNU/Linux host.
- KVM nested vitalization enabled: [See this guide](https://docs.fedoraproject.org/en-US/quick-docs/using-nested-virtualization-in-kvm/).
- [libvirt](https://libvirt.org/) and [vagrant](https://www.vagrantup.com/) installed.
- The `vagrant-hostmanager` plugin (`vagrant plugin install vagrant-hostmanager`).
- The `vagrant-reload` plugin (`vagrant plugin install vagrant-reload`).
- An SSH public key available at `~/.ssh/id_rsa.pub`.

### Vagrant basics

If you are new to vagrant, we strongly advice you to read the official
[introduction to vagrant](https://www.vagrantup.com/intro/index.html).
It is also recommended that you read the official
[getting started](https://www.vagrantup.com/intro/getting-started/index.html)
guide.

### Getting started

- Provisioning VMs.
  ```
  vagrant up
  ```
  Note that preparing the VMs may take a very long time. You can check the
  status of the all vagrant VMs using the following command:
  ```
  vagrant global-status
  ```
  It is not necessary for you to wait until all machines are in state ready to
  continue, but expect lags when using them (e.g. running ansible) before they
  are ready.

- Destroying any trace of the provisioned VMs. This command may be used if the
  `Vagrantfile` has been modified.
  ```
  vagrant destroy
  ```
  Note that you will have to manually remove the entries related to the vagrant
  hosts from your `~/.ssh/known_hosts` file when you recreate the VMs. See the
  vagrant files for getting the IP address.

- You can reboot the VMs (e.g. after a kernel upgrade) using the following command:
  ```
  vagrant reload
  ```

- You can manage and attach to the server console using `virt-manager`.

You can test your machines are up and running with the following command: `vagrant ssh vgagepsrv0`.

When a new set of VMs are created, your SSH public key is automatically added
to the root account. The following command may be run to configure your user
account in all hosts.

Go in the root of ansible directory
```
ansible -i inventories/vagrant -u root 'vgagepsrv*' -m include_role -a name=accounts -e "@kryptonite/accounts/users.yml
```

When running this command for the first time in a new VM, depending on your
hardware it may take up to 2 minutes to run without any sign of progress.
This happens only the first time, successive runs take no more than a couple of
seconds.
