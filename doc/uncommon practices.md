# Uncommon practices and why (WIP)


# Common practices enforced by ansible-lint that have been silenced

## (503) Task that run when changed should likely be handlers

Ansible does not support block handlers nor provides any other way to
sequentially chain commands in a handler.
For example, when configuring a network interface we want to make sure that the
configuration is valid before restarting said interface.
Since we cannot perform the configuration validation __and__ the interface
configuration sequentially by a handler, we must keep them imperative in the
role.

Note that using pub/sub triggers is not acceptable since they are casted to the
global scope (e.g. `foo-handler` defined in role `foo` may be called from a
different role).


## (306) Shells that use pipes should set the pipefail option

`pipefail` is not POSIX and there is no guarantee that the system shell is bash.
